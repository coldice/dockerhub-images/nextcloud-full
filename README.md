[Git Repository](https://gitlab.com/coldice/dockerhub-images/nextcloud-full)

# Nextcloud Full
This image is based on the [official](https://hub.docker.com/_/nextcloud) Nextcloud image and extends it with packages that are required by the most common apps. To avoid the need of running multiple containers for Nextcloud specific background jobs like cron and OCR scan, [Supervisor](http://supervisord.org/) is used as an init process.

## Changes
Most of the changes are based on the [full example](https://github.com/nextcloud/docker/tree/master/.examples/dockerfiles/full) from the official Nextcloud Docker repository. The additions roughly are:

- Added `Supervisor` to handle running Apache, cron and OCR live scanner
- Added `bz2` library to extract apps
- Added `IMAP` libraries to authenticate users via IMAP accounts
- Added `Samba` libraries for external storages
- Added `ffmpeg` and `imagemagick` SVG libraries for proper previews
- Added `tesseract` including all available languages for OCR

## Full Example (docker-compose)
Use this as an example and create your own configuration for the images. Never not use `latest` image tags for production.

### Nextcloud stack
In order to run Elasticsearch and Postgres optimal, the following sysctl settings must be set on the host:
- `vm.swappiness=1`
- `vm.max_map_count=262144`

```yaml
version: "3"
services:
  # This service mounts and provides WebDAV storage to Nextcloud
  storage:
    image: cldc/webdav-storage-mount:latest
    restart: unless-stopped
    environment:
      - DAV_URL=https://my.webdav-share.tld
      - DAV_USERNAME=user1
      - DAV_PASSWORD=secure-pa55word
      # Nextcloud uses www-data as user
      - DAV_UID=33
      - DAV_GID=33
    devices:
      # To create a fuse mount this is required
      - /dev/fuse
    cap_add:
      # To create a fuse mount this is required
      - SYS_ADMIN
    security_opt:
      # To create a fuse mount this is required if AppArmor is used
      - apparmor=unconfined
    volumes:
      # rshared is required to propagate the mount to host
      - /mnt/nextcloud/data:/mnt:rshared

  app:
    image: cldc/nextcloud-full:latest
    restart: unless-stopped
    depends_on:
      - db
      - redis
      - storage
      - elasticsearch
    environment:
      # Look up those settings on Nextcloud repositories
      - NEXTCLOUD_DATA_DIR=/nextcloud/data
      - POSTGRES_HOST=db
      - POSTGRES_DB=nextcloud
      - POSTGRES_USER=nextcloud
      - POSTGRES_PASSWORD=nextcloud
      - NEXTCLOUD_TRUSTED_DOMAINS=nextcloud.my-domain.tld
      - REDIS_HOST=redis
      - SMTP_HOST=smtp.my-provider.tld
      - SMTP_SECURE=ssl
      - SMTP_NAME=nextcloud@my-provider.tld
      - SMTP_PASSWORD=secure-pa55word
      - MAIL_FROM_ADDRESS=nextcloud
      - MAIL_DOMAIN=my-provider.tld
      # Those are important if you use a reverse proxy in docker (see Traefik example)
      - APACHE_DISABLE_REWRITE_IP=1
      - TRUSTED_PROXIES=172.16.0.0/12
      - NC_overwriteprotocol=https
    networks:
      # proxy_public is created by the Traefik stack
      - proxy_public
      - backend
    volumes:
      - nextcloud:/var/www/html
      # rslave is important here to propagate the WebDAV mount back to the container
      - /mnt/nextcloud:/nextcloud:rslave
    labels:
      # For Traefik configuration please use the according documentation
      - "traefik.enable=true"
      - "traefik.docker.network=proxy_public"
      - "traefik.http.routers.nextcloud.entrypoints=https"
      - "traefik.http.routers.nextcloud.rule=Host(`nextcloud.my-domain.tld`)"
      - "traefik.http.routers.nextcloud.middlewares=dav-redirect,hsts"
      - "traefik.http.middlewares.dav-redirect.replacepathregex.regex=^/.well-known/(card|cal)dav"
      - "traefik.http.middlewares.dav-redirect.replacepathregex.replacement=/remote.php/dav/"
      - "traefik.http.middlewares.hsts.headers.referrerPolicy=no-referrer"
      - "traefik.http.middlewares.hsts.headers.stsSeconds=31536000"
      - "traefik.http.middlewares.hsts.headers.forceSTSHeader=true"
      - "traefik.http.middlewares.hsts.headers.stsPreload=true"
      - "traefik.http.middlewares.hsts.headers.stsIncludeSubdomains=true"
      - "traefik.http.middlewares.hsts.headers.browserXssFilter=true"

  # Elasticsearch is used for the Full Text Search app to index text
  elasticsearch:
    image: cldc/es-ingest-attachment:latest
    restart: unless-stopped
    stop_grace_period: 5m
    environment:
      # Will wait for cluster join without this setting
      - discovery.type=single-node
      - bootstrap.memory_lock=true
    ulimits:
      memlock:
        soft: -1
        hard: -1
    networks:
      - backend
    volumes:
      - elasticsearch:/usr/share/elasticsearch/data

  # Database for Nextcloud
  db:
    image: postgres:latest
    restart: unless-stopped
    stop_grace_period: 5m
    environment:
      - POSTGRES_USER=nextcloud
      - POSTGRES_PASSWORD=nextcloud
    networks:
      - backend
    volumes:
      - db:/var/lib/postgresql/data

  # External and file lock cache for Nextcloud
  redis:
    image: redis:latest
    restart: unless-stopped
    networks:
      - backend

  # TURN server for Nextcloud Talk and peers behind NAT
  turn:
    image: instrumentisto/coturn:latest
    restart: unless-stopped
    command:
      - -n
      - --no-cli
      # Change the port to a non-standard if you like, don't forget to match up ports section
      - --listening-port=3478
      - --no-tls
      - --no-dtls
      - --min-port=49160
      - --max-port=49200
      - --log-file=stdout
      - --external-ip=$$(detect-external-ip)
      - --fingerprint
      - --use-auth-secret
      # Use something like that: openssl rand -hex 48
      - --static-auth-secret=long-hex-string-with-would-be-nice
      - --realm=nextcloud.my-domain.tld
      - --total-quota=100
      - --bps-capacity=0
      - --stale-nonce
      - --no-multicast-peers
    tmpfs:
      # This image uses an anonymous volume here for persistence of sessions - we make this a tmpfs to avoid it (no need)
      - /var/lib/coturn
    networks:
      - backend
    ports:
      - 3478:3478
      - 3478:3478/udp
      - 49160-49200:49160-49200
      - 49160-49200:49160-49200/udp

volumes:
  nextcloud:
  db:
  elasticsearch:

networks:
  proxy_public:
    external: true
  backend:
```

### Traefik stack
The docker-compose project is named `proxy`, that's the reason for the namespace as in the network `proxy_public`.

```yaml
version: "3"
services:
  traefik:
    image: traefik:latest
    restart: unless-stopped
    command:
      - --entrypoints.http.address=:80
      - --entrypoints.http.http.redirections.entrypoint.to=https
      - --entrypoints.https.address=:443
      - --entrypoints.https.http.tls.certresolver=default
      - --certificatesresolvers.default.acme.email=me@my-provider.tld
      - --certificatesresolvers.default.acme.storage=/etc/traefik/acme/acme.json
      - --certificatesresolvers.default.acme.dnschallenge.provider=cloudflare
      - --certificatesresolvers.default.acme.dnschallenge.resolvers=1.1.1.1:53,1.0.0.1:53
      - --providers.docker.exposedbydefault=false
      - --providers.docker.network=proxy_public
    environment:
      # The created API token must have access to view all zones and edit DNS (see docs)
      - CF_DNS_API_TOKEN=my-cloudflare-api-token
    networks:
      - public
    ports:
      - 80:80
      - 443:443
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - traefik:/etc/traefik/acme

volumes:
  traefik:

networks:
  public:
```

## Example cloud-init for Hetzner
This cloud-init config for example can be used to create a server on Hetzner Cloud for Nextcloud using a Stroage Box as storage backend for data. Set SSH keys and server name on deployment by the UI / CLI or Terraform.

```yaml
#cloud-config
apt:
  sources:
    docker.list:
      source: "deb [arch=amd64] https://download.docker.com/linux/ubuntu $RELEASE stable"
      keyid: 0EBFCD88

package_upgrade: true
package_reboot_if_required: true

packages:
  - docker-ce
  - docker-ce-cli
  - containerd.io
  # Following required for Ansible
  - python3-pip
  - python3-setuptools
  - python3-wheel

swap:
  filename: /swapfile
  size: auto

write_files:
  - path: /etc/sysctl.d/60-cloudinit.conf
    content: |
      vm.swappiness=1
      vm.max_map_count=262144

bootcmd:
  - sysctl vm.swappiness=1
  - sysctl vm.swappiness=262144
  # Use a custom SSH port
  - sed -i "/Port 22/c\Port 2222" /etc/ssh/sshd_config

runcmd:
  - ufw limit 57622/tcp
  - ufw enable
  # Following required for Ansible
  - pip3 install docker-compose docker
```